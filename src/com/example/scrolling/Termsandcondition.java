package com.example.scrolling;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;



import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by Mavin apps on 4/29/2015.
 */
public class Termsandcondition extends Activity {

    ImageView Backtohometerms;
    TextView txt_accept, txt_cancel;
    ScrollView scrollView;
    TextView ending;

    //Response
    ArrayList<NameValuePair> registerrequestParams;
    boolean acceptsview = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.termsandcondition);

        Backtohometerms = (ImageView) findViewById(R.id.back_btns);
        txt_accept = (TextView) findViewById(R.id.txt_accept);
        ending=(TextView)findViewById(R.id.ending);

        txt_cancel = (TextView) findViewById(R.id.txt_cancel);
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent backtohome = new Intent(Termsandcondition.this, MainActivity.class);
                startActivity(backtohome);
                overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
                finish();
            }
        });

        scrollView = (ScrollView) findViewById(R.id.scr_view_terms);
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {
                Rect scrollBounds = new Rect();
                scrollView.getHitRect(scrollBounds);
                if (ending.getLocalVisibleRect(scrollBounds)) {
                    txt_accept.setBackgroundColor(getResources().getColor(R.color.default_color));
                    txt_cancel.setBackgroundColor(getResources().getColor(R.color.darkgray));
                    acceptsview = true;
                } else {
                    // imageView is not within the visible window
                }

            }
        });

    }

   

   
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


}
