package com.example.scrolling;

import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends Activity {

	Button first;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        first=(Button)findViewById(R.id.btn_con);
        first.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent first=new Intent(MainActivity.this, Termsandcondition.class);
				startActivity(first);
				finish();
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
		});
    }


   
}
